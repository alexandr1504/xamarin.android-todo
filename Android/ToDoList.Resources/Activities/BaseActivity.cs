﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using Calligraphy;

namespace ToDoList.Resources.Activities
{
    public abstract class BaseActivity : AppCompatActivity, View.IOnClickListener
    {
        private Toolbar toolbar;
       
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(LayoutResource);

        }

        protected abstract int LayoutResource { get; }

        public Toolbar Toolbar
        {
            set
            {
                toolbar = value;
                SetSupportActionBar(toolbar);
                Toolbar.SetNavigationOnClickListener(this);
                SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                SupportActionBar.SetDisplayShowTitleEnabled(false);
                SupportActionBar.SetHomeButtonEnabled(true);
            }
            get { return toolbar; }
        }


        protected override void AttachBaseContext(Context newBase)
        {
            base.AttachBaseContext(CalligraphyContextWrapper.Wrap(newBase));
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;

            return base.OnOptionsItemSelected(item);
        }

        public void OnClick(View v)
        {
            this.OnBackPressed();
        }
    }
}