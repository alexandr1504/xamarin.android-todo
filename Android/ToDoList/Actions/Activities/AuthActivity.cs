﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using ToDoList.Presenter;
using Android.Support.Design.Widget;
using Android.Graphics;
using Android.Content.PM;

using ToDoList.Generic.Models;
using ToDoList.Resources.Activities;
using ToDoList.DataModel;
using System.Threading.Tasks;

namespace ToDoList.Actions.Activities
{
    [Activity(
        Label ="",
        Theme = "@style/AppThemeBase",
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class AuthActivity : BaseActivity
    {
        private AuthPresenter auth_pres;

        private LinearLayout loginUser, regUser, auth_form;
        private EditText email_login, pswd_login, email_reg, pswd_reg, username_reg;
        private Button reg_view, login_view, Login_user, Register_user;

        protected override int LayoutResource => Resource.Layout.Auth_layout;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            auth_pres = new AuthPresenter();

            auth_form = FindViewById<LinearLayout>(Resource.Id.auth_form);
            loginUser = FindViewById<LinearLayout>(Resource.Id.login_user);
            regUser = FindViewById<LinearLayout>(Resource.Id.register_user);
            regUser.Visibility = ViewStates.Gone;

            email_login = FindViewById<EditText>(Resource.Id.email_view);
            pswd_login = FindViewById<EditText>(Resource.Id.pswd_view);
            email_reg = FindViewById<EditText>(Resource.Id.register_email_view);
            pswd_reg = FindViewById<EditText>(Resource.Id.register_pswd_view);
            username_reg = FindViewById<EditText>(Resource.Id.username_view);

            reg_view = FindViewById<Button>(Resource.Id.set_register);
            login_view = FindViewById<Button>(Resource.Id.set_login);
            Login_user = FindViewById<Button>(Resource.Id.Login_user);
            Register_user = FindViewById<Button>(Resource.Id.Register_user);

            reg_view.Click += Handle_Click;
            login_view.Click += Handle_Click;
            Login_user.Click += Handle_Click;
            Register_user.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.set_register))
                {
                    loginUser.Visibility = ViewStates.Gone;
                    regUser.Visibility = ViewStates.Visible;
                    auth_pres.IsLoginView = false;

                }
                else if (button.Id.Equals(Resource.Id.set_login))
                {
                    loginUser.Visibility = ViewStates.Visible;
                    regUser.Visibility = ViewStates.Gone;
                    auth_pres.IsLoginView = true;

                }
                else if (button.Id.Equals(Resource.Id.Login_user))
                {
                    if (IsFoolField)
                    {
                        SendLogin();
                    }
                    else
                        ViewAttention();

                }
                else if (button.Id.Equals(Resource.Id.Register_user))
                {
                    if (IsFoolField)
                    {
                        SendRegister();                       
                    }
                    else
                        ViewAttention();


                }
            }
        }

        private bool IsFoolField
        {
            get
            {
                if (auth_pres.IsLoginView)
                {
                    return (email_login.Text.Length > 3 && pswd_login.Text.Length > 3) ? true : false;
                }
                else
                    return (username_reg.Text.Length > 3 && email_reg.Text.Length > 3 && pswd_reg.Text.Length > 3) ? true : false;

            }
        }

        private void ViewAttention()
        {
            //var alert = Toast.MakeText(this, msg_phone, ToastLength.Long);
            //alert.SetGravity(GravityFlags.CenterVertical & GravityFlags.CenterHorizontal, 0, 0);
            //alert.Show();
            var attention = GetString(Resource.String.attention_auth_title);
            var view = FindViewById(Android.Resource.Id.Content);
            var snackbar = Snackbar.Make(view, attention, Snackbar.LengthLong);
            var snackbarView = snackbar.View;
            snackbarView.SetBackgroundColor(Color.BlueViolet);
            snackbar.Show();
        }

        private async void SendLogin()
        {
            var model = new LoginViewModel
            {
                Email = email_login.Text,
                Password = pswd_login.Text
            };

            await Task.WhenAll(auth_pres.SendLogin(model)).ContinueWith(t =>
            {
                auth_pres.SetLocalUser();
                ClearForm(0);
                if (auth_pres.IsSuccess)
                    Finish();
            });
        }

        private async void SendRegister()
        {
            var user = new Users
            {
                UserName = username_reg.Text,
                Email = email_reg.Text,
                Password = pswd_reg.Text
            };

            await Task.WhenAll(auth_pres.SendRegister(user)).ContinueWith(t => 
            {
                auth_pres.SetLocalUser();
                ClearForm(1);
                if (auth_pres.IsSuccess)
                    Finish();
            });
        }

        private void ClearForm(int flag)
        {
            this.RunOnUiThread(() =>
            {
                switch (flag)
                {
                    case 0:
                        email_login.Text = "";
                        pswd_login.Text = "";
                        break;
                    case 1:
                        email_reg.Text = "";
                        pswd_reg.Text = "";
                        username_reg.Text = "";
                        break;
                }
            });
            
        }

    }
}


