﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ToDoList.Resources.Activities;
using ToDoList.Presenter;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Content.PM;
using Android.Content.Res;
using ToDoList.Actions.Views;

namespace ToDoList.Actions.Activities
{
    [Activity(
        LaunchMode = LaunchMode.SingleTask,
        Label = "@string/app_name", 
        Theme = "@style/AppTheme", 
        Icon = "@drawable/icon", 
        MainLauncher = true,
        WindowSoftInputMode = SoftInput.AdjustResize,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class LayoutActivity : BaseActivity
    {
        private LayoutPresenter l_presenter;
        private DrawerLayout drawerLayout;
        private NavigationView navigationView;
        private View navHeaderView;
        private Button signBtn;
        private TextView userName;

        TaskListView myTasksView;


        protected override int LayoutResource => Resource.Layout.Main;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            Init();
        }

        private void Init()
        {
            l_presenter = new LayoutPresenter();

            myTasksView = FindViewById<TaskListView>(Resource.Id.tasks_view);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.main_drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            Toolbar = myTasksView.FindViewById<Toolbar>(Resource.Id.home_toolbar2);
            signBtn = FindViewById<Button>(Resource.Id.btn_sing_in);


            var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, Toolbar, Resource.String.drawer_open, Resource.String.drawer_close);
            drawerLayout.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();
            navHeaderView = navigationView.InflateHeaderView(Resource.Layout.Menu_header_view);
            userName = navHeaderView.FindViewById<TextView>(Resource.Id.header_title);

            if (!l_presenter.CheckUser)
            {
                var intent = new Intent(this, typeof(AuthActivity));
                this.StartActivityForResult(intent, 0);
            }
            else
            {
                myTasksView.InitObj();
                SetMenu();
            }

            navigationView.NavigationItemSelected += Handle_NavigationItemSelected;
            signBtn.Click += Handle_Click;
        }

        private void SetMenu()
        {
            navigationView.Menu.Clear();
            navigationView.InflateMenu(Resource.Menu.Navigation_menu);
            navigationView.Menu.GetItem(0).SetChecked(true);

            SetHeaderView();
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            switch (requestCode)
            {
                case 0:
                    l_presenter.GetUser();
                    myTasksView.InitObj();
                    SetMenu();
                    break;
                case 1:

                    if (data != null)
                    {
                       var str = data.GetStringExtra("Task");

                        myTasksView.UpdateTask(str);
                    }

                    break;
                case 2:
                    navigationView.SetCheckedItem(Resource.Id.nav_x);
                    l_presenter.GetUser();
                    SetHeaderView();
                    break;

            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.btn_sing_in))
                {
                    l_presenter.Logout();
                    var intent = new Intent(this, typeof(AuthActivity));
                    this.StartActivityForResult(intent, 0);
                }
            }
        }

        private void Handle_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            var menuItem = e.MenuItem;

            menuItem.SetChecked(menuItem.IsChecked);
            drawerLayout.CloseDrawers();

            switch (menuItem.ItemId)
            {
                case (Resource.Id.nav_x):

                    break;
                case (Resource.Id.nav_x2):
                    var intent = new Intent(this, typeof(SettingsActivity));
                    StartActivityForResult(intent,2);
                    break;

            }
        }

        void SetHeaderView()
        {
            if (l_presenter.User.UserName == null)
                userName.Text = l_presenter.User.Email;
            else
                userName.Text = l_presenter.User.UserName;
        }

    }
}