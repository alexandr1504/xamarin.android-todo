﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using ToDoList.Resources.Activities;
using ToDoList.Presenter;
using Android.Support.Design.Widget;
using Android.Graphics;
using ToDoList.DataModel;

namespace ToDoList.Actions.Activities
{
    [Activity(Theme = "@style/AppTheme")]
    public class SettingsActivity : BaseActivity
    {
        SettingsPresenter stg_presenter;
        FrameLayout paswdForm;
        Button changePswd, save_password;
        EditText u_nm, u_email, cur_pswd,new_pswd;
        protected override int LayoutResource => Resource.Layout.Settings_view;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            stg_presenter = new SettingsPresenter();

            Toolbar = FindViewById<Toolbar>(Resource.Id.home_toolbar);
            paswdForm = FindViewById<FrameLayout>(Resource.Id.change_password_form);
            changePswd = FindViewById<Button>(Resource.Id.set_change_password);
            save_password = FindViewById<Button>(Resource.Id.action_save_password);
            u_nm = FindViewById<EditText>(Resource.Id.et_edit_username_view);
            u_email = FindViewById<EditText>(Resource.Id.et_editemail_view);
            cur_pswd = FindViewById<EditText>(Resource.Id.current_password);
            new_pswd = FindViewById<EditText>(Resource.Id.new_password);

            Toolbar.Title = Resources.GetString(Resource.String.todo_settings);
            paswdForm.Visibility = ViewStates.Gone;

            InitView();

            changePswd.Click += Handle_Click;
            save_password.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (button != null)
            {
                if (button.Id.Equals(Resource.Id.set_change_password))
                {
                    if (paswdForm.Visibility == ViewStates.Gone)
                        paswdForm.Visibility = ViewStates.Visible;
                    else
                        paswdForm.Visibility = ViewStates.Gone;
                }
                else if (button.Id.Equals(Resource.Id.action_save_password))
                {
                    if (cur_pswd.Text != null && stg_presenter.User.Password != null && stg_presenter.User.Password.Equals(cur_pswd.Text))
                    {
                        var model = new Users
                        {
                            Id = stg_presenter.User.Id,
                            Password = new_pswd.Text
                        };
                        stg_presenter.UpdateLocalProfile(model);
                        stg_presenter.UpdateServerProfile(model);
                        paswdForm.Visibility = ViewStates.Gone;
                        this.Finish();
                    }
                    else
                        ShowAttention();

                }
            }


        }

        public override void OnBackPressed()
        {
            if (stg_presenter.IsChanged(u_nm.Text, u_email.Text))
            {
                var model = new Users
                {
                    Id = stg_presenter.User.Id,
                    UserName = u_nm.Text,
                    Email = u_email.Text,
                };
                stg_presenter.UpdateLocalProfile(model);
                stg_presenter.UpdateServerProfile(model);

            }

            base.OnBackPressed();
        }

        void InitView()
        {
            if (stg_presenter.User.UserName != null)
                u_nm.Text = stg_presenter.User.UserName;
            if (stg_presenter.User.Email != null)
                u_email.Text = stg_presenter.User.Email;

        }


        private void ShowAttention()
        {
            var attention = GetString(Resource.String.attention_wrong_password);
            var view = FindViewById(Android.Resource.Id.Content);
            var snackbar = Snackbar.Make(view, attention, Snackbar.LengthLong);
            var snackbarView = snackbar.View;
            snackbarView.SetBackgroundColor(Color.Blue);
            snackbar.Show();
        }

    }
}