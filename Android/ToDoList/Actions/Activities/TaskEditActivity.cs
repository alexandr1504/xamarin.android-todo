﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

using ToDoList.Resources.Activities;
using ToDoList.Presenter;
using ToDoList.Actions.Views;
using Android.Views.InputMethods;
using Newtonsoft.Json;

namespace ToDoList.Actions.Activities
{
    [Activity(Theme = "@style/AppTheme")]
    public class TaskEditActivity : BaseActivity
    {
        TaskEditPresenter te_presenter;
        FrameLayout dateFrame, taskFrame;
        ImageButton delTask, delEndDate;
        ToggleButton switchTask;
        EditText task_nameEdit, taskNote;
        TextView title_taskName, taskCreated, taskEndDate;
        View doneView;
        protected override int LayoutResource => Resource.Layout.Task_edit_view;

        public InputMethodManager InputManager => (InputMethodManager)this.GetSystemService(Context.InputMethodService);


        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            te_presenter = new TaskEditPresenter();


            var task = Intent.GetStringExtra("Task");

            if (task != null)
            {
                te_presenter.SetTask(task);
            }

            Toolbar = FindViewById<Toolbar>(Resource.Id.home_toolbar);
            dateFrame = FindViewById<FrameLayout>(Resource.Id.task_date_select_frame);
            taskFrame = FindViewById<FrameLayout>(Resource.Id.task_text_edit_frame);
            switchTask = FindViewById<ToggleButton>(Resource.Id.toggle_switch_task);
            task_nameEdit = FindViewById<EditText>(Resource.Id.et_task_name);
            taskNote = FindViewById<EditText>(Resource.Id.et_note_view);
            taskCreated = FindViewById<TextView>(Resource.Id.title_created_at);
            taskEndDate = FindViewById<TextView>(Resource.Id.task_date_select);
            title_taskName = FindViewById<TextView>(Resource.Id.tv_task_name);
            doneView = FindViewById<View>(Resource.Id.task_done_view);
            delTask = FindViewById<ImageButton>(Resource.Id.ib_del_task);
            delEndDate = FindViewById<ImageButton>(Resource.Id.ib_del_end_date);

            SetViewData();

            switchTask.Click += Handle_Click;
            taskFrame.Click += Handle_Click;
            dateFrame.Click += Handle_Click;
            delTask.Click += Handle_Click;
            delEndDate.Click += Handle_Click;


        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var frmae_button = sender as FrameLayout;
            var img_button = sender as ImageButton;
            var togle_btn = sender as ToggleButton;

            if (frmae_button != null)
            {
                if (frmae_button.Id.Equals(Resource.Id.task_date_select_frame))
                {
                    var dlg = new TaskdateDialogView(this);
                    dlg.Show();
                    dlg.DismissEvent += Handle_DismissEvent;
                }
                else if (frmae_button.Id.Equals(Resource.Id.task_text_edit_frame))
                {
                    if(task_nameEdit.Visibility == ViewStates.Gone)
                    {
                        if (doneView.Visibility == ViewStates.Visible)
                            doneView.Visibility = ViewStates.Gone;

                        var text_nm = title_taskName.Text;
                        title_taskName.Visibility = ViewStates.Gone;
                        task_nameEdit.Visibility = ViewStates.Visible;
                        task_nameEdit.Text = text_nm;
                        task_nameEdit.RequestFocus();
                        InputManager.ShowSoftInput(task_nameEdit, ShowFlags.Implicit);
                    }

                }
            }
            else if (img_button != null)
            {
                if (img_button.Id.Equals(Resource.Id.ib_del_task))
                {
                    var returnIntent = new Intent();
                    returnIntent.PutExtra("Task", "rm");
                    SetResult(Result.Ok, returnIntent);
                    Finish();
                }
                else if (img_button.Id.Equals(Resource.Id.ib_del_end_date))
                {
                    te_presenter.TaskModel.DueDate = null;
                    var title = Resources.GetString(Resource.String.add_task_date);
                    taskEndDate.Text = title;
                    img_button.Visibility = ViewStates.Gone;
                }
            }
            else if (togle_btn != null)
            {
                SetSwitchTask(togle_btn.Checked);
            }
            
        }

        private void Handle_DismissEvent(object sender, EventArgs e)
        {
            var dialog = sender as TaskdateDialogView;

            te_presenter.TaskModel.DueDate = dialog.Date.ToShortDateString();

            SetEndDateView();
        }

        public override void OnBackPressed()
        {
            SaveChanges();

            base.OnBackPressed();
        }

        private void SetViewData()
        {
            var model = te_presenter.TaskModel;

            if (te_presenter.TaskModel != null )
            {
                if (model.Title != null)
                    title_taskName.Text = model.Title;

                if (model.Note != null)
                    taskNote.Text = te_presenter.TaskModel.Note;

                SetEndDateView();

                var cr = Resources.GetString(Resource.String.todo_created_at);

                taskCreated.Text = cr + " : " + model.CreatedAt;

                SetSwitchTask(model.IsDone);

                switchTask.Checked = model.IsDone;
            }
        }

        private void SetEndDateView()
        {
            if (te_presenter.TaskModel.DueDate != null && te_presenter.TaskModel.DueDate.Length > 0)
            {
                taskEndDate.Text = te_presenter.TaskModel.DueDate;

                if (delEndDate.Visibility == ViewStates.Gone)
                    delEndDate.Visibility = ViewStates.Visible;
            }
        }
        private void SetSwitchTask(bool flag)
        {
            switch (flag)
            {
                case true:
                    string tsk_nm = "";

                    if (task_nameEdit.Visibility == ViewStates.Visible)
                    {
                        tsk_nm = task_nameEdit.Text;
                        title_taskName.Visibility = ViewStates.Visible;
                        task_nameEdit.Visibility = ViewStates.Gone;
                        title_taskName.Text = tsk_nm;
                    }

                    title_taskName.Post(() =>
                    {
                        var w = title_taskName.Width;
                        doneView.Visibility = ViewStates.Visible;
                        var lp = doneView.LayoutParameters;
                        lp.Width = w;
                        doneView.LayoutParameters = lp;
                    });
                   
                    //InputManager.HideSoftInputFromWindow(this.Window.DecorView.RootView.WindowToken, 0);
                    te_presenter.TaskModel.IsDone = true;

                    break;
                case false:
                    doneView.Visibility = ViewStates.Gone;
                    te_presenter.TaskModel.IsDone = false;
                    break;
            }
        }

        private void SaveChanges()
        {
            var t_nm = "";
            if (task_nameEdit.Text.Length > 0)
            {
                t_nm = task_nameEdit.Text;
            }
            else
               t_nm = title_taskName.Text;

            var t_note = taskNote.Text;

            te_presenter.UpdateModel(t_nm, t_note);

            var data = JsonConvert.SerializeObject(te_presenter.TaskModel);
            var returnIntent = new Intent();
            returnIntent.PutExtra("Task", data);
            SetResult(Result.Ok, returnIntent);
        }

    }
}