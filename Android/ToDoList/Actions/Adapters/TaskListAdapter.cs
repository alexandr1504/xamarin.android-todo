﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

using ToDoList.Generic.Models;
using ToDoList.DataModel;

namespace ToDoList.Actions.Adapters
{
    public class TaskListAdapter : RecyclerView.Adapter
    {
        private Context cx;

        public TaskListAdapter(Context cx, List<Tasks> items) : base()
        {
            this.cx = cx;
            DataHolder = items;
        }

        public List<Tasks> DataHolder { get; set; }


        public event EventHandler<int> ItemClick;

        public delegate void ArgsEventHandler(object sender, int e,object arg);


        public event ArgsEventHandler HandleEvent;

        void OnClick(int position)
        {
            if (ItemClick != null)
                ItemClick(this, position);
        }

        void OnHandleEvent(object sender, int position, object arg)
        {
            if (HandleEvent != null)
                HandleEvent(sender, position, arg);
        }

        public override int ItemCount => DataHolder.Count;

        public override long GetItemId(int position)
        {
            return base.GetItemId(position);
        }
        public override int GetItemViewType(int position)
        {
            return base.GetItemViewType(position);
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            holder.IsRecyclable = true;
            var viewholder = holder as TaskListViewHolder;

            var data = DataHolder[position];

            viewholder.TaskName.Text = data.Title;
            viewholder.SwitchTask.Checked = data.IsDone;
            viewholder.ShowDoneLine(data.IsDone);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var id = Resource.Layout.Task_list_adapter_view;
            var itemView = LayoutInflater.From(parent.Context).Inflate(id, parent, false);

            return new TaskListViewHolder(itemView, OnHandleEvent, OnClick);
        }

    }

    class TaskListViewHolder : RecyclerView.ViewHolder
    {
        private View view;
        private TextView taskName;
        private ToggleButton switchTask;
        private View doneView;
        public TextView TaskName => taskName;
        public ToggleButton SwitchTask => switchTask;

        public TaskListViewHolder(View itemView, Action<object, int,object> handleEvent, Action<int> clickEvent) : base(itemView)
        {
            view = itemView;

            taskName = view.FindViewById<TextView>(Resource.Id.task_name);
            switchTask = view.FindViewById<ToggleButton>(Resource.Id.toggle_switch_task);
            doneView = view.FindViewById<View>(Resource.Id.task_done_view);

            view.Tag = this;
            view.Id = Position;
            view.Click += (sender, e) => clickEvent(base.Position);
            switchTask.Click += (sender, e) => handleEvent(sender, Position, e);
        }


        public void ShowDoneLine(bool flag)
        {
            switch (flag)
            {
                case true:
                    taskName.Post(() =>
                    {
                        var w = taskName.Width;
                        doneView.Visibility = ViewStates.Visible;
                        var lp = doneView.LayoutParameters;
                        lp.Width = w;
                        doneView.LayoutParameters = lp;
                    });
                    break;
                case false:
                    doneView.Visibility = ViewStates.Gone;
                    break;
            }
        }


    }

}
