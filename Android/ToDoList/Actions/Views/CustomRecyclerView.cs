﻿
using Android.Content;
using Android.Support.V7.Widget;
using Android.Util;

namespace ToDoList.Actions.Views
{
    public class CustomRecyclerView : RecyclerView
    {
        public CustomRecyclerView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Initialize();
        }

        public CustomRecyclerView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            Initialize();
        }

        private void Initialize()
        {

        }

        public override bool Fling(int velocityX, int velocityY)
        {
            velocityY = velocityY / 2;

            return base.Fling(velocityX, velocityY);
        }
    }
}