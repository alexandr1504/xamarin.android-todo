﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views.InputMethods;
using Android.Text;
using Android.Support.V4.Widget;

using ToDoList.Presenter;
using ToDoList.Actions.Adapters;
using ToDoList.Actions.Activities;
using Newtonsoft.Json;
using ToDoList.Generic.Models;
using ToDoList.DataModel;
using System.Threading.Tasks;

namespace ToDoList.Actions.Views
{
    public class TaskListView : FrameLayout
    {
        TaskListPresenter t_presenter;
        Context cx;
        RecyclerView myTasks;
        TaskListAdapter tasksAdapter;
        LinearLayoutManager tasks_lManager;
        FrameLayout add_taskFrame;
        FloatingActionButton show_form;
        EditText add_task;
        ImageButton newTask_btn;
        TextView emptyListview;
        CollapsingToolbarLayout clps_toolbar;
        NestedScrollView scrollContainer;
        AppBarLayout barLayout;
        SwipeRefreshLayout refresher;

        bool isShowkeyboard = false;
        bool canChangeResource = true;
        int selectedItem;
        public InputMethodManager InputManager => (InputMethodManager)this.Context.GetSystemService(Context.InputMethodService);

        public TaskListView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            cx = context;
            Initialize();
        }

        public TaskListView(Context context, IAttributeSet attrs, int defStyle) : base(context, attrs, defStyle)
        {
            cx = context;
            Initialize();
        }

        private void Initialize()
        {
            Inflate(cx, Resource.Layout.Task_list_view, this);

            t_presenter = new TaskListPresenter();

            myTasks = FindViewById<RecyclerView>(Resource.Id.mytaskList_view);
            emptyListview = FindViewById<TextView>(Resource.Id.empty_view);
            add_taskFrame = FindViewById<FrameLayout>(Resource.Id.frm_new_taskView);
            add_task = FindViewById<EditText>(Resource.Id.ebx_taskView);
            newTask_btn = FindViewById<ImageButton>(Resource.Id.buttonDone);
            show_form = FindViewById<FloatingActionButton>(Resource.Id.fab_showform_taskView);
            clps_toolbar = FindViewById<CollapsingToolbarLayout>(Resource.Id.collapsing_toolbar);
            barLayout = FindViewById<AppBarLayout>(Resource.Id.appbar);
            scrollContainer = FindViewById<NestedScrollView>(Resource.Id.scroll_container_taskView);
            refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
            

            newTask_btn.Enabled = false;
            clps_toolbar.Title = Resources.GetString(Resource.String.menu_title_todo_list);

            tasks_lManager = new LinearLayoutManager(cx, LinearLayoutManager.Vertical, false);
            myTasks.SetLayoutManager(tasks_lManager);

            

            add_task.ImeOptions = ImeAction.Done;
            add_task.SetRawInputType(InputTypes.ClassText);
            
            refresher.Refresh += HandleRefresh;
            add_task.TextChanged += Handle_TextChanged;
            add_task.Click += Handle_Click;
            show_form.Click += Handle_Click;
            newTask_btn.Click += Handle_Click;
            add_task.EditorAction += Handle_EditorAction;
            add_task.FocusChange += Handle_FocusChange;
        }

        private void OnHandleEvent(object sender, int e,object arg)
        {
            var toggle_button = sender as ToggleButton;

            if (toggle_button != null)
            {
                var check = toggle_button.Checked;

                if (t_presenter.TodoTasks[e].IsDone != check)
                {
                    t_presenter.TodoTasks[e].IsDone = check;
                    t_presenter.UpdateLocaleTask(e,null);

                    tasksAdapter.NotifyItemChanged(e);
                }

            }
        }

        private void Handle_ItemClick(object sender, int e)
        {
            selectedItem = e;
            var item = JsonConvert.SerializeObject(t_presenter.TodoTasks[e]);
            var intent = new Intent(cx, typeof(TaskEditActivity));
            intent.PutExtra("Task", item);
            ((Activity)cx).StartActivityForResult(intent,1);
        }

        async void HandleRefresh(object sender, EventArgs e)
        {

            await Task.WhenAll(t_presenter.DownloadData()).ContinueWith(t =>
            {
                ((Activity)cx).RunOnUiThread(() =>
                {
                    tasksAdapter.NotifyDataSetChanged();
                });
            });
           
            refresher.Refreshing = false;
        }
        private void Handle_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            switch (e.ActionId)
            {
                case ImeAction.Done:
                    if(newTask_btn.Enabled && !IsTextSpace(add_task.Text))
                        AddToTask();

                    break;
            }
        }

        private void Handle_TextChanged(object sender, TextChangedEventArgs e)
        {
            var edittext = sender as EditText;
            var str = edittext.Text;

            if (str.Length > 0 && canChangeResource)
            {
                newTask_btn.SetImageResource(Resource.Drawable.ic_add_task);
                canChangeResource = false;
                newTask_btn.Enabled = true;
            }
            else if(str.Length==0)
            {
                newTask_btn.SetImageResource(Resource.Drawable.ic_add_task_empty);
                canChangeResource = true;
                newTask_btn.Enabled = false;
            }
        }

        private void Handle_FocusChange(object sender, FocusChangeEventArgs e)
        {
            switch (e.HasFocus)
            {
                case true:
                    barLayout.SetExpanded(false);
                    break;
                case false:
                    isShowkeyboard = false;
                    InputManager.HideSoftInputFromWindow(this.WindowToken, 0);

                    break;
            }
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            var fab = sender as FloatingActionButton;
            var img_button = sender as ImageButton;

            if (fab != null)
            {
                if (fab.Id.Equals(Resource.Id.fab_showform_taskView))
                {
                    show_form.Visibility = ViewStates.Gone;
                    add_taskFrame.Visibility = ViewStates.Visible;
                    isShowkeyboard = true;
                    add_task.RequestFocus();
                    InputManager.ShowSoftInput(add_task, ShowFlags.Implicit);
                }
            }
            else if (img_button != null)
            {
                if (img_button.Id.Equals(Resource.Id.buttonDone))
                {
                    var task_name = add_task.Text;

                    if (task_name.Length > 0 && !IsTextSpace(task_name))
                    {
                        AddToTask();
                    }
                }
            }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            base.OnMeasure(widthMeasureSpec, heightMeasureSpec);

            var proposedheight = MeasureSpec.GetSize(heightMeasureSpec);
            var actualHeight = Height;

            if (actualHeight > proposedheight)
            {
                // Keyboard is shown
                isShowkeyboard = true;
            }
            else
            {
                if (isShowkeyboard && actualHeight != proposedheight)
                {
                    show_form.Visibility = ViewStates.Visible;
                    add_taskFrame.Visibility = ViewStates.Gone;
                }
            }


        }

        private void IsListEmpty()
        {
            if (t_presenter.TodoTasks.Count == 0)
                emptyListview.Visibility = ViewStates.Visible;
            else
                emptyListview.Visibility = ViewStates.Gone;
        }

        private bool IsTextSpace(string str)
        {
            var c = new char[str.Length];

            for (int i = 0; i < str.Length; i++)
                c[i] = ' ';

            return str.Equals(new string(c)) ? true : false;
        }

        private void AddToTask()
        {
            if (emptyListview.Visibility != ViewStates.Gone)
                emptyListview.Visibility = ViewStates.Gone;

            var task_name = add_task.Text;

            t_presenter.AddTask(task_name);

            tasksAdapter.NotifyItemChanged(t_presenter.TodoTasks.Count - 1);

            myTasks.Post(() =>
            {
                myTasks.ScrollToPosition(t_presenter.TodoTasks.Count - 1);
            });
            add_task.Text = "";
            newTask_btn.Enabled = false;
        }

        public void UpdateTask(string obj)
        {
            if (obj.Equals("rm"))
            {
                t_presenter.RemoveTask(selectedItem);
                IsListEmpty();

                tasksAdapter.NotifyDataSetChanged();

            }
            else
            {
                var data = JsonConvert.DeserializeObject<Tasks>(obj);

                t_presenter.UpdateLocaleTask(selectedItem, data);

                myTasks.Post(() => tasksAdapter.NotifyItemChanged(selectedItem));
            }
        }

        public void InitObj()
        {
            t_presenter.InitObj();
            tasksAdapter = new TaskListAdapter(cx, t_presenter.TodoTasks);
            myTasks.SetAdapter(tasksAdapter);
            IsListEmpty();
            tasksAdapter.ItemClick += Handle_ItemClick;
            tasksAdapter.HandleEvent += OnHandleEvent;

        }

    }
}