﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace ToDoList.Actions.Views
{
    public class TaskdateDialogView : Dialog
    {
        private Context cx;
        DatePicker taskDt;
        Button readyBtn;
        public TaskdateDialogView(Context context) : base(context)
        {
            this.cx = context;

            Initialize();
        }

        private void Initialize()
        {
            SetContentView(Resource.Layout.Task_date_dialog_view);

            taskDt = FindViewById<DatePicker>(Resource.Id.task_datechooser);
            readyBtn = FindViewById<Button>(Resource.Id.action_ready);

            var title = ((Activity)cx).Resources.GetString(Resource.String.todo_date);

            this.SetTitle(title);

            readyBtn.Click += Handle_Click;
        }

        private void Handle_Click(object sender, EventArgs e)
        {
            this.Dismiss();
        }

        public DateTime Date => taskDt.DateTime;
    }
}