﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Newtonsoft.Json;

using ToDoList.Generic.Models;
using ToDoList.Generic.ViewModels;
using ToDoList.DataModel;
using ToDoList.Generic.DAL.Repository;
using System.IO;

namespace ToDoList.Presenter
{
    public class AuthPresenter : BasePresenter
    {
        private RestCall api;
        private ContentAccess contentDb;
        private string requestRes;

        public AuthPresenter()
        {
            api = RestCall.Instance(Configuration.BaseUrl);
            contentDb = ContentAccess.Instance(ConnectionString);

            IsLoginView = true;
            IsSuccess = false;
        }

        public bool IsLoginView { get; set; }

        public bool IsSuccess { get; set; }

        public Users User { get; set; }

        public async Task SendRegister(Users user)
        {
            await Task.Run(async () =>
            {
                requestRes = await api.RequestPost(Configuration.Register, user);

                if (requestRes != Configuration.ErrorExecute)
                {
                    User = JsonConvert.DeserializeObject<Users>(requestRes);
                }
            });
        }

        public async Task SendLogin(LoginViewModel model)
        {
            await Task.Run(async () =>
            {
                requestRes = await api.RequestPost(Configuration.Login, model);

                if (requestRes != Configuration.ErrorExecute)
                {
                    User = JsonConvert.DeserializeObject<Users>(requestRes);
                }

            });
        }

        public void SetLocalUser()
        {
            if (User.Email != null)
            {
                contentDb.SetUser(User);
                IsSuccess = true;
            }
        }

    }
}
