﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using ToDoList.Generic.Models;

namespace ToDoList.Presenter
{
    public abstract class BasePresenter
    {

        public string ConnectionString
        {
            get
            {
                string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

                return Path.Combine(documentsPath, Configuration.sqliteFilename);
            }
        }



    }
}