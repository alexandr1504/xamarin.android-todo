﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using ToDoList.Generic.DAL.Repository;
using ToDoList.DataModel;
using ToDoList.Generic.Models;
using System.Threading.Tasks;
using ToDoList.Generic.DAL.DataModel;
using ToDoList.Generic.ViewModels;

namespace ToDoList.Presenter
{
    public class LayoutPresenter : BasePresenter
    {
        ContentAccess accessDb;
        RestCall api;
        public LayoutPresenter()
        {

            accessDb = ContentAccess.Instance(ConnectionString);
            api = RestCall.Instance(Configuration.BaseUrl);

            GetUser();
        }

        public Users User { get; private set; }

        public bool CheckUser => User != null ? true : false;

        public void Logout()
        {
            accessDb.RemoveUser(User);
        }

        public void GetUser()
        {
            User = accessDb.User;
        }

        //void EmptyCach()
        //{
        //    if (Configuration.IsConnected)
        //    {
        //        var cache =  local_cache.GetAllFromCache<CachedObject>();

        //        foreach (var item in cache)
        //        {
        //            Task.Run(async () => await api.RequestPost(item.ApiUri, item.Value));
        //        }
        //        local_cache.ClearCache();
        //    }
        //}
    }
}