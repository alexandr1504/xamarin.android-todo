﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using ToDoList.Generic.Models;
using ToDoList.Generic.ViewModels;
using ToDoList.Generic.DAL.Repository;
using ToDoList.DataModel;
using System.Threading.Tasks;
using ToDoList.Repository;
using ToDoList.DataAccess;

namespace ToDoList.Presenter
{
    public class SettingsPresenter : BasePresenter
    {
        RestCall api;
        private IGenericRepository<Users> db;

        string requestRes;

        public SettingsPresenter()
        {
            api = RestCall.Instance(Configuration.BaseUrl);
            db = new GenericRepository<Users>(DbConnection.Instance(ConnectionString));

            User = db.SelectAll().SingleOrDefault();
        }

        public Users User { get; private set; }

        public void UpdateLocalProfile(Users model)
        {
            db.UpdateItem(model);
        }

        public void UpdateServerProfile(Users model)
        {
            Task.Run(async () =>
            {
                var path = "/Api/Profile/Update";
                requestRes = await api.RequestPost(path, model);

            });

        }

        public bool IsChanged(string u_nm,string u_email)
        {
            return ((u_nm.Length > 0 && u_nm != User.UserName) || (u_email.Length
                > 0 && u_email != User.Email)) ? true : false;
        }

    }
}