﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ToDoList.Generic.Models;
using Newtonsoft.Json;
using ToDoList.DataModel;

namespace ToDoList.Presenter
{
    public class TaskEditPresenter
    {
        public TaskEditPresenter()
        {

        }

        public Tasks TaskModel { get; set; }

        public string DueDate { get; set; }

        public void SetTask(string str)
        {
            TaskModel = JsonConvert.DeserializeObject<Tasks>(str);
        }

        public void UpdateModel(string t_nm,string t_note)
        {
            TaskModel.Title = t_nm;
            TaskModel.Note = t_note;

            if (DueDate != null)
            {
                TaskModel.DueDate = DueDate;
            }
        }

    }
}