﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using Newtonsoft.Json;

using ToDoList.Generic.Models;
using ToDoList.DataModel;
using ToDoList.Generic.ViewModels;
using ToDoList.Generic.DAL.Repository;
using ToDoList.Resources;

namespace ToDoList.Presenter
{
    public class TaskListPresenter : BasePresenter
    {
        RestCall api;
        ContentAccess db_cx;

        List<Tasks> toDo_tasks;
        List<Tasks> toUpdate;

        string requestRes;

        public TaskListPresenter()
        {
            toDo_tasks = new List<Tasks>();
            toUpdate = new List<Tasks>();
        }

        public void InitObj()
        {
            api = RestCall.Instance(Configuration.BaseUrl);
            db_cx = new ContentAccess(ConnectionString);

            AutoMapper.Mapper.CreateMap<Tasks, TaskModel>();

            User = db_cx.UserInfo.SelectAll().SingleOrDefault();

            NewTS();
            UpdateTS();
            RemoveTS();
            SetTaskArr();

        }
        public Users User { get; set; }

        public List<Tasks> TodoTasks
        {
            set { toDo_tasks = value; }
            get { return toDo_tasks; }
        }

        public List<Tasks> ToUpdate { get; set; }
        void SetTaskArr()
        {
            if (TodoTasks.Count > 0)
                TodoTasks.Clear();

            TodoTasks = db_cx.MyTasks.SelectAll().Where(t => t.Task_state != "r").ToList();
        }

        public void AddTask(string title)
        {
            var task = new Tasks
            {
                UserId = User.Id,
                Title = title,
                IsDone = false,
                Task_state = "n",
                CreatedAt = DateTime.Now.ToShortDateString(),
            };

            TodoTasks.Add(task);

            db_cx.MyTasks.InsertItem(task);
        }

        public void UpdateLocaleTask(int ind, Tasks data)
        {
            if (data != null)
            {
                if (data.Title != null && data.Title != TodoTasks[ind].Title)
                    TodoTasks[ind].Title = data.Title;
                if (data.Note != null && data.Note != TodoTasks[ind].Note)
                    TodoTasks[ind].Note = data.Note;
                if (data.IsDone != TodoTasks[ind].IsDone)
                    TodoTasks[ind].IsDone = data.IsDone;
                if (data.DueDate != null && data.DueDate != TodoTasks[ind].DueDate)
                    TodoTasks[ind].DueDate = data.DueDate;

                //TodoTasks[ind] = data;
            }
            if(TodoTasks[ind].Task_state!="n")
            {
                TodoTasks[ind].Task_state = "u";
            }
            db_cx.MyTasks.UpdateDataAsync(TodoTasks[ind]);
        }

        public void RemoveTask(int ind)
        {
            if(TodoTasks[ind].Task_state == "n")
            {
                db_cx.MyTasks.DeleteRow(TodoTasks[ind]);
            }
            else
            {
                TodoTasks[ind].Task_state = "r";
                db_cx.MyTasks.UpdateItem(TodoTasks[ind]);
            }
            TodoTasks.RemoveAt(ind);

        }

        public async void NewTS()
        {
            var tsks = db_cx.MyTasks.SelectAll().Where(x=>x.Task_state == "n").ToList();

            if (Configuration.IsConnected && tsks.Count>0)
            {
                await NewTaskServer(tsks);
            }
        }

        public async void UpdateTS()
        {
            var tsks = db_cx.MyTasks.SelectAll().Where(x => x.Task_state.Equals("u")).ToList();

            //tsks.ForEach(t =>
            //{
            //    if (t.ServerId == 0)
            //    {
            //        t.Task_state = "n";
            //        db_cx.MyTasks.UpdateItem(t);
            //    }
            //});

            tsks.RemoveAll(x => x.ServerId == 0);

            for (int i = 0; i < tsks.Count; i++)
            {
                if(tsks[i].ServerId!=0)
                    tsks[i].Id = tsks[i].ServerId;
            }

            if (Configuration.IsConnected && tsks.Count > 0)
            {
                await UpdateTaskServer(tsks);
            }
        }

        public async void RemoveTS()
        {
            var tsks = db_cx.MyTasks.SelectAll().Where(x => x.Task_state.Equals("r")).ToList();

            tsks.RemoveAll(x => x.ServerId == 0);

            for (int i = 0; i < tsks.Count; i++)
            {
                if (tsks[i].ServerId != 0)
                    tsks[i].Id = tsks[i].ServerId;
            }


            if (Configuration.IsConnected && tsks.Count > 0)
            {
               await RemoveTaskServer(tsks);
            }
        }

        public async Task DownloadData()
        {
            requestRes = await api.RequestGet(Configuration.DownloadTasks + User.Id);
            if (requestRes != Configuration.ErrorExecute)
            {
                var downloaded_tasks = JsonConvert.DeserializeObject<List<Tasks>>(requestRes);

                if (downloaded_tasks.Count > 0)
                {
                    downloaded_tasks.ForEach(t =>
                    {
                        t.ServerId = t.Id;
                        t.Id = 0;
                        t.Task_state = "-";
                    });

                    toUpdate = downloaded_tasks.Where(p => !TodoTasks.Any(p2 => p2.Title == p.Title)).ToList();

                    db_cx.MyTasks.InsertList(toUpdate);

                    toUpdate.ForEach(t =>
                    {
                        TodoTasks.Add(t);
                    });
                }
            }
        }

        Task NewTaskServer(List<Tasks> model)
        {
            var new_tasks = model;
            var map_task = AutoMapper.Mapper.Map<List<Tasks>, List<TaskModel>>(new_tasks);

           var t = Task.Run(async () =>
            {

                requestRes = await api.RequestBodyPost(Configuration.Newtask, map_task);

                if (requestRes != Configuration.ErrorExecute)
                {
                    var id_task = JsonConvert.DeserializeObject<List<int>>(requestRes);

                    if (id_task.Count > 0)
                    {
                        for (int i = 0; i < new_tasks.Count; i++)
                        {
                            new_tasks[i].ServerId = id_task[i];
                            new_tasks[i].Task_state = "-";
                            db_cx.MyTasks.UpdateItem(new_tasks[i]);
                        }

                        new_tasks.ForEach(x =>
                        {
                            var id = TodoTasks.FindIndex(ind => ind.Id.Equals(x.Id));

                            if (id > 0)
                                TodoTasks[id] = x;
                        });
                    }
                }

            });

            return t;
        }

        Task UpdateTaskServer(List<Tasks> model)
        {
            var update_tasks = model;

            var t = Task.Run(async () =>
            {
                requestRes = await api.RequestBodyPost(Configuration.Updatetask, model);

                if (requestRes != Configuration.ErrorExecute)
                {
                    var tsks = db_cx.MyTasks.SelectAll().Where(x => x.Task_state.Equals("u")).ToList();

                    foreach (var item in tsks)
                    {
                        item.Task_state = "-";
                        db_cx.MyTasks.UpdateItem(item);
                    }

                    tsks.ForEach(x =>
                    {
                        var id = TodoTasks.FindIndex(ind => ind.Id.Equals(x.Id));

                        if (id > 0)
                            TodoTasks[id] = x;
                    });
                }
            });

            return t;
        }
        Task RemoveTaskServer(List<Tasks> model)
        {
            var tsks = db_cx.MyTasks.SelectAll().Where(x => x.Task_state.Equals("r")).ToList();

            var t =  Task.Run(async() =>
            {
                requestRes = await api.RequestBodyPost(Configuration.Removetask, model);

                if (requestRes != Configuration.ErrorExecute)
                {
                    foreach (var item in tsks)
                    {
                        db_cx.MyTasks.DeleteRow(item);
                    }
                }
            });

            return t;
        }

    }
}



