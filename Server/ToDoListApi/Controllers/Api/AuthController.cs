﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ToDoListApi.CoreSql;
using ToDoListApi.Models;
using ToDoListApi.Services;
using Users = ToDoListApi.Models.Users;

namespace ToDoListApi.Controllers.Api
{
    public class AuthController : ApiController
    {
        private CoreDB db;
        private readonly AuthService _authService = new AuthService();
        public AuthController()
        {
            db = new CoreDB(DBase.СonnectString);
        }

        [HttpPost]
        public IHttpActionResult Login(LoginViewModel model)
        {
            var user = _authService.GetByEmail(model.Email, model.Password);

            var data = new Users
            {
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                Password = user.Password,
                CreatedAt = user.CreatedAt
            };

            return Json(data);
        }


        [HttpPost]
        public IHttpActionResult Register(Users user)
        {
            _authService.CreateUser(user);
            var new_user = _authService.GetByEmail(user.Email, user.Password);

            var data = new Users
            {
                Id = new_user.Id,
                UserName = new_user.UserName,
                Email = new_user.Email,
                Password = new_user.Password,
                CreatedAt = new_user.CreatedAt
            };

            return Json(data);
        }

    }
}
