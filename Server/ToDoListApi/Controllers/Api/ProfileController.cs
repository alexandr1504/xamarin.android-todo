﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using LinqKit;
using ToDoListApi.Models;
using ToDoListApi.Services;

namespace ToDoListApi.Controllers.Api
{
    public class ProfileController : ApiController
    {
        private readonly ProfileService _profileService = new ProfileService();
        private IMapper mapper;

        public ProfileController()
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Users, UserViewModel>();
            });

            mapper = config.CreateMapper();
        }

        [HttpPost]
        public IHttpActionResult Update(Users model)
        {
            var user = _profileService.GetById(model.Id);

            if (model.UserName != null)
                user.UserName = model.UserName;
            if (model.Email != null)
                user.Email = model.Email;
            if (model.Password != null)
                user.Password = model.Password;

            _profileService.UpdateRow(user);

            return Ok();
        }


        [HttpGet]
        public IHttpActionResult FindProfile(string toFind)
        {
            var search = Predicate(toFind);
            var items = _profileService.FindBy(search).ToList();

            var map = mapper.Map<List<Users>, List<UserViewModel>>(items);

            return Json(map);
        }

        private Expression<Func<Users, bool>> Predicate(string filter)
        {
            var predicate = PredicateBuilder.True<Users>();

            return predicate.And(p => p.ToString().Contains(filter));
        }


    }
}
