﻿using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using ToDoListApi.Models;
using ToDoListApi.Services;

namespace ToDoListApi.Controllers.Api
{
    public class TasksController : ApiController
    {
        private CoreSql.CoreDB db;

        private readonly TasksSevice _taskService = new TasksSevice();

        public TasksController()
        {
            db = new CoreSql.CoreDB(DBase.СonnectString);
        }


        [HttpPost]
        public IHttpActionResult AddTask([FromBody]List<Tasks> model)
        {
            var data = new List<Tasks>();

            foreach (var item in model)
            {
                var d = _taskService.CreateTask(item);
                data.Add(d);
            }

            return Json(data.Select(x=>x.Id).ToList());
        }


        [HttpPost]
        public HttpResponseMessage UpdateTask([FromBody]List<Tasks> model)
        {
            foreach (var item in model)
            {
                var e = _taskService.GetById(item.Id);

                if (e != null)
                {
                    if (item.Title != null && item.Title != e.Title)
                        e.Title = item.Title;
                    if (item.Note != null && item.Note != e.Note)
                        e.Note = item.Note;
                    if (item.IsDone != e.IsDone)
                        e.IsDone = item.IsDone;
                    if (item.DueDate != null && item.DueDate != e.DueDate)
                        e.DueDate = item.DueDate;

                    _taskService.UpdateRow(e);
                }
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPost]
        public HttpResponseMessage RemoveTask([FromBody]List<Tasks> model)
        {
            foreach (var item in model)
            {
                var e = _taskService.GetById(item.Id);
                if(e!=null)
                    _taskService.DeleteRow(e);
            }

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpGet]
        public IHttpActionResult FindByWeek(int userId)
        {
            var ts = new List<TaskModel>();
            var now = DateTime.Now;
            var myCal = CultureInfo.InvariantCulture.Calendar;
            var fromD = myCal.AddDays(now, -7);

            var tasks = _taskService.GetAllById(userId);

            tasks.ForEach(x =>
            {
                if (Convert.ToDateTime(x.CreatedAt).Date >= fromD)
                    ts.Add(new TaskModel
                    {
                        Id = x.Id,
                        UserId = x.UserId,
                        CreatedAt = x.CreatedAt,
                        DueDate = x.DueDate,
                        IsDone = x.IsDone,
                        Title = x.Title,
                        Note = x.Note
                    });

            });
           

            return Ok(ts);
        }

        private Expression<Func<Tasks, bool>> Predicate(int userid)
        {
            var predicate = PredicateBuilder.True<Tasks>();

            return predicate.And(p => p.Id.Equals(userid));
        }       

    }
}

