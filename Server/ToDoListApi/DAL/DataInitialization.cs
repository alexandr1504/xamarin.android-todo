﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoListApi.Models;

namespace ToDoListApi.DAL
{
    public class DataInitialization : DropCreateDatabaseIfModelChanges<ToDoContext>
    {
        protected override void Seed(ToDoContext context)
        {
            var users = new List<Users>
            {
                new Users
                {
                     UserName = "Alex",
                     Email = "alex@gmail.com",
                     Password = "qwerty",
                     CreatedAt = DateTime.Now.ToShortDateString()
                },
                new Users
                {
                     UserName = "Ivan",
                     Email = "ivan@gmail.com",
                     Password = "qwerty",
                     CreatedAt = DateTime.Now.ToShortDateString()
                },
                new Users
                {
                     UserName = "Iva",
                     Email = "iva@gmail.com",
                     Password = "qwerty",
                     CreatedAt = DateTime.Now.ToShortDateString()
                }
            };

            users.ForEach(c => context.Users.Add(c));
            context.SaveChanges();
        }
    }
}