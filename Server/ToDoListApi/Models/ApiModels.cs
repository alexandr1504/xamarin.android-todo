﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoListApi.Models
{

    public struct DBase
    {
        public static string СonnectString = System.Configuration.ConfigurationManager.ConnectionStrings["ToDoContext"].ToString();
    }
    public struct ServerURLs
    {
        //public static string SignalURI = "http://localhost:49328/";
        public static string SignalURI = "http://192.168.0.100:49328/";
    }

    public class UserViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] Pic { get; set; }
    }

    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }


    public class TaskModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public bool IsDone { get; set; }
        public string CreatedAt { get; set; }
        public string DueDate { get; set; }
    }

}