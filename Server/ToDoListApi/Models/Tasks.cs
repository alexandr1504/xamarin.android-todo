﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoListApi.Models
{
    public class Tasks
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Note { get; set; }
        public bool IsDone { get; set; }
        [Required]
        public string CreatedAt { get; set; }
        public string DueDate { get; set; }

        public virtual Users User { get; set; }
    }
}