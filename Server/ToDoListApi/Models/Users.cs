﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ToDoListApi.Models
{
    public class Users
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string CreatedAt { get; set; }

        public virtual ICollection<Tasks> Tasks { get; set; }

        public override string ToString()
        {
            return Id + ' ' + UserName + ' ' + Email;
        }
    }
}