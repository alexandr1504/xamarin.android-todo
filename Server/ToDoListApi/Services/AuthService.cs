﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ToDoListApi.DAL;
using ToDoListApi.Models;

namespace ToDoListApi.Services
{
    public class AuthService : IDisposable
    {
        private ToDoContext _db = new ToDoContext();

        public Users GetById(int userId)
        {
            return _db.Users.SingleOrDefault(ci => ci.Id == userId);
        }

        public Users GetByEmail(string email, string password)
        {
            return _db.Users.SingleOrDefault(us => us.Email == email && us.Password == password);
        }

        public Users CreateUser(Users user)
        {
            _db.Entry(user).State = EntityState.Added;
            _db.SaveChanges();

            var item = GetById(user.Id);

            return item;
        }

        public void UpdateRow(Users user)
        {
            _db.Entry(user).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void DeleteUser(Users user)
        {
            _db.Entry(user).State = EntityState.Deleted;
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}