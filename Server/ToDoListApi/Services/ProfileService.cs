﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ToDoListApi.DAL;
using ToDoListApi.Models;

namespace ToDoListApi.Services
{
    public class ProfileService
    {
        private ToDoContext _db = new ToDoContext();

        public Users GetById(int userId)
        {
            return _db.Users.SingleOrDefault(ci => ci.Id == userId);
        }

        public void UpdateRow(Users user)
        {
            _db.Entry(user).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public IEnumerable<Users> FindBy(Expression<Func<Users, bool>> predicate)
        {
            return _db.Users.Where(predicate.Compile());
        }


    }
}