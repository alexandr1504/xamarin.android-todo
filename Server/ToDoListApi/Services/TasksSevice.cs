﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ToDoListApi.DAL;
using ToDoListApi.Models;

namespace ToDoListApi.Services
{
    public class TasksSevice
    {
        private ToDoContext _db = new ToDoContext();

        public TasksSevice()
        {

        }

        public DbSet<Tasks> DbTask => _db.Tasks;
        public Tasks GetById(int taskId)
        {
            return _db.Tasks.SingleOrDefault(ci => ci.Id == taskId);
        }

        public List<Tasks> GetAllById(int userId)
        {
            return _db.Tasks.Where(ci => ci.UserId == userId).Select(t=>t).ToList();
        }

        public List<Tasks> GetAll()
        {
            return _db.Tasks.ToList();
        }
        public Tasks CreateTask(Tasks task)
        {
            _db.Entry(task).State = EntityState.Added;
            _db.SaveChanges();

            var item = _db.Tasks.ToList().LastOrDefault();

            return item;
        }

        public void UpdateRow(Tasks task)
        {
            _db.Entry(task).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void DeleteRow(Tasks task)
        {
            _db.Tasks.Remove(task);
            _db.SaveChanges();
        }

        public void DeleteRange(List<Tasks> tasks)
        {
            _db.Tasks.RemoveRange(tasks);
            _db.SaveChanges();
        }

        //public List<Tasks> FindBy(Expression<Func<Tasks, bool>> predicate)
        //{
        //    return _db.Tasks.Where(predicate.Compile()).ToList();
        //}
    }
}