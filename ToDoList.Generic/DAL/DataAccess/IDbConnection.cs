﻿using SQLite;

namespace ToDoList.DataAccess
{
    public interface IDbConnection
    {
        SQLiteAsyncConnection GetAsyncConnection { get; }
        SQLiteConnection GetConnection { get; }
        bool IsExistDB { get; }
    }
}
