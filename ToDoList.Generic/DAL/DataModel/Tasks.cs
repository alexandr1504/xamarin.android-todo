﻿using System;
using System.Collections.Generic;
using System.Text;

using SQLite;

namespace ToDoList.DataModel
{
    [Table("Tasks")]
    public class Tasks
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }
        public int ServerId { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public bool IsDone { get; set; }
        public string CreatedAt { get; set; }
        public string DueDate { get; set; }
        public string Task_state { get; set; }
    }
}
