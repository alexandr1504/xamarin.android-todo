﻿
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace ToDoList.DataModel
{
    [Table("Users")]
    public class Users
    {
        [PrimaryKey]
        public int Id { get; set; }
        [MaxLength(30)]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CreatedAt { get; set; }
    }
}
