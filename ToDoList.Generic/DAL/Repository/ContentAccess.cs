﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ToDoList.DataAccess;
using ToDoList.DataModel;
using ToDoList.Repository;

namespace ToDoList.Generic.DAL.Repository
{
    public class ContentAccess
    {
        private static ContentAccess instance;
        private IGenericRepository<Users> db;
        private IGenericRepository<Tasks> task_cx;

        private Users user;

        public ContentAccess(string dbPath)
        {
            db = new GenericRepository<Users>(DbConnection.Instance(dbPath));
            task_cx = new GenericRepository<Tasks>(DbConnection.Instance(dbPath));

        }

        public static ContentAccess Instance(string dbPath)
        {
           return instance == null ? instance = new ContentAccess(dbPath) : instance;
        }


        public IGenericRepository<Users> UserInfo => db;

        public IGenericRepository<Tasks> MyTasks => task_cx;

        public Users User => db.SelectAll().SingleOrDefault();


        public void SetUser(Users data)
        {
            db.InsertItem(data);
        }


        public void RemoveUser(Users data)
        {
            db.DeleteRow(data);
        }

    }
}
