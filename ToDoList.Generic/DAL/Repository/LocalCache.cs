﻿using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ToDoList.Repository;
using ToDoList.DataAccess;
using ToDoList.Generic.DAL.DataModel;

namespace ToDoList.Generic.DAL.Repository
{
    public class LocalCache
    {
        private static CacheConfiguration Configuration;
        private SQLiteConnection Db => Configuration.Connection;

        public LocalCache(string dbPath)
        {
            Configuration = CacheConfiguration.Instance(DbConnection.Instance(dbPath));
        }

        public void WriteToCache<T>(string key, T data, string api, DateTimeOffset timeStamp)
        {
            if (String.IsNullOrEmpty(key) || data == null || timeStamp == DateTimeOffset.MinValue) return;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            if (currentValue == null)
            {

                var item = new CachedObject
                {
                    Key = key,
                    UpdatedAt = timeStamp,
                    Value = JsonConvert.SerializeObject(data),
                    ApiUri = api
                };
                Db.Insert(item);
            }
            else
            {
                currentValue.Value = JsonConvert.SerializeObject(data);
                currentValue.UpdatedAt = timeStamp;
                currentValue.ApiUri = api;
                Db.Update(currentValue);
            }
        }

        public DateTimeOffset CacheLastUpdated(string key)
        {
            if (String.IsNullOrEmpty(key)) return DateTimeOffset.MinValue;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();
            return currentValue?.UpdatedAt ?? DateTimeOffset.MinValue;
        }

        public void RemoveCache(string key)
        {
            if (String.IsNullOrEmpty(key)) return;

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            if (currentValue == null) return;

            Db.Delete(currentValue);
        }

        public T GetFromCache<T>(string key)
        {
            if (String.IsNullOrEmpty(key)) return default(T);

            var currentValue = Db.Table<CachedObject>().Where(o => o.Key == key).ToList().FirstOrDefault();

            return currentValue?.Value == null ? default(T) : JsonConvert.DeserializeObject<T>(currentValue.Value);
        }

        public List<T> GetAllFromCache<T>() where T : class, new()
        {
            return Db.Table<T>().ToList();
        }

        public void ClearCache()
        {
            Db.DeleteAll<CachedObject>();
            //Db.DropTable<CachedObject>();
        }

    }

    public class CacheConfiguration
    {
        private SQLiteConnection connection;
        private static CacheConfiguration instance;
        public CacheConfiguration(IDbConnection oIDbConnection)
        {
            connection = oIDbConnection.GetConnection;
            connection.CreateTable<CachedObject>();
        }

        public static CacheConfiguration Instance(IDbConnection oIDbConnection)
        {
            if (instance == null)
                instance = new CacheConfiguration(oIDbConnection);

            return instance;
        }

        public SQLiteConnection Connection => connection;

    }
}


