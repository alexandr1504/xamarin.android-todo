﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.Generic.Models
{

    public class UserViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] Pic { get; set; }
    }

    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }


    public class TaskModel
    {
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public bool IsDone { get; set; }
        public string CreatedAt { get; set; }
        public string DueDate { get; set; }
    }


}
