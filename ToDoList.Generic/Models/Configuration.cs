﻿using Plugin.Connectivity;
using System;

namespace ToDoList.Generic.Models
{
    public static class Configuration
    {
        public const string BaseUrl = "http://192.168.0.102:54586/";
        public const string sqliteFilename = "TodoListDB.db3";
        public static readonly string Newtask = "/Api/Tasks/AddTask";
        public static readonly string Updatetask = "/Api/Tasks/UpdateTask";
        public static readonly string Removetask = "/Api/Tasks/RemoveTask";
        public static readonly string DownloadTasks = "/Api/Tasks/FindByWeek/?userId=";
        public static readonly string Login = "/Api/Auth/Login";
        public static readonly string Register = "/Api/Auth/Register";

        
        public static readonly string ErrorExecute = "Error";

        public static bool IsConnected => CrossConnectivity.Current.IsConnected;

    }
}
