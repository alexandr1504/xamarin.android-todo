﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoList.Generic.Helpers;

namespace ToDoList.Generic.ViewModels
{
    public class FilterData<T>
    {
        public FilterData(List<T> model)
        {
            Data = model;
        }
        public List<T> Data { get; private set; }

        public List<T> SearchFilter(params string[] keywords)
        {
            var predicate = PredicateBuilder.True<T>();

            foreach (string keyword in keywords)
            {
                predicate = predicate.And(p => p.ToString().Contains(keyword));
            }
            return Data.Where(predicate.Compile()).ToList();
        }

    }
}
