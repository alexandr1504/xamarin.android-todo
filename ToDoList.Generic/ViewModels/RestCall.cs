﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ToDoList.Generic.Models;

namespace ToDoList.Generic.ViewModels
{
    public class RestCall
    {
        private static RestCall instance;
        private RestClient client;
        public RestCall(string baseUrl)
        {
            client = new RestClient(baseUrl);
        }

        public static RestCall Instance(string baseUrl)
        {
            return instance==null?instance = new RestCall(baseUrl): instance;
        }

        public async Task<string> RequestPost(string api, object data)
        {
            var request = new RestRequest(api, Method.POST);
            request.AddObject(data);
            //request.AddJsonBody(data);
            var cancellationTokenSource = new CancellationTokenSource();

            var response = await client.ExecuteTaskAsync(request, cancellationTokenSource.Token);

            if (response.ErrorException != null || response.StatusCode == HttpStatusCode.InternalServerError)
                    return response.ErrorException.Message;

            return response.Content;
        }


        public async Task<string> RequestBodyPost(string api, object data)
        {
            var request = new RestRequest(api, Method.POST);
            request.AddJsonBody(data);
            var cancellationTokenSource = new CancellationTokenSource();

            var response = await client.ExecuteTaskAsync(request, cancellationTokenSource.Token);

            if (response.ErrorException != null || response.StatusCode == HttpStatusCode.InternalServerError)
                return Configuration.ErrorExecute;

            return response.Content;
        }

        public async Task<string> RequestGet(string api)
        {
            var request = new RestRequest(api, Method.GET);
            request.RequestFormat = DataFormat.Json;

            var cancellationTokenSource = new CancellationTokenSource();

            var response = await client.ExecuteTaskAsync(request, cancellationTokenSource.Token);
            //var response = client.Execute(request);

            if(response.ErrorException != null || response.StatusCode == HttpStatusCode.InternalServerError)
                return Configuration.ErrorExecute;

            return response.Content;
        }


    }
}
